// const firebase = require('firebase');

// var config = {
//   apiKey: "AIzaSyBiV4pW0JlP-Z3eXj3ocirxw0axjQfIO1E",
//   authDomain: "gdg-firefest.firebaseapp.com",
//   databaseURL: "https://gdg-firefest.firebaseio.com",
//   projectId: "gdg-firefest",
//   storageBucket: "",
//   messagingSenderId: "725404606775"
// };

// firebase.initializeApp(config);

// const ref = firebase.database().ref();

// exports.middleware = (store) => (next) => (action) => {
//   if ('SESSION_USER_DATA' === action.type) {
//     const { data } = action;

//     console.log(escape(data));

//     // ref.push().set({
//     //   command: escape(action.data)
//     // });
//   }

//   next(action);
// }

const firebase = require('firebase');

const config = {
  apiKey: "AIzaSyDsV2CyNmko4AB2Tmmr06P5eJYxO3l1QAM",
  authDomain: "hyper-firebase.firebaseapp.com",
  databaseURL: "https://hyper-firebase.firebaseio.com",
  projectId: "hyper-firebase",
  storageBucket: "hyper-firebase.appspot.com",
  messagingSenderId: "670723844205"
};

firebase.initializeApp(config);
const ref = firebase.database().ref('terms');
const notificationsRef = firebase.database().ref('notifications');

exports.middleware = (store) => (next) => (action) => {
  if ('SESSION_ADD_DATA' === action.type) {
    var { data } = action;

    if (/command/.test(data)) {
      notificationsRef.push().set(data);
    } else {
      var mydata = data.split(/\n/g).join(':').replace(/\:.+/g, '');

      if (!mydata.includes('[') && !mydata.includes(']')) {
        notificationsRef.push().set(mydata);
      }
    }
  }

  next(action);
}

exports.decorateTerm = (Term, { React }) => class extends React.Component {
  render() {
    return React.createElement(Term, Object.assign({}, this.props, {
      onTerminal: (term) => {
        if (this.props && this.props.onTerminal) {
          this.props.onTerminal(term);
        }

        ref.on('child_added', snap => {
          term.io.sendString(snap.val());
        });
      }
    }));
  }
};

// exports.middleware = (store) => (next) => (action) => {
//   if ('SESSION_USER_DATA' === action.type) {
//     const { data } = action;

//     console.log(escape(data));

//     if (/\%0D/.test(escape(data))) {
//       console.log(123);
//       next({
//         type: 'SESSION_ADD_DATA',
//         data: ''
//       });
//     }
//   }

//   next(action);
// }